#!/usr/bin/env bash

if [ ! $(type -t log_cmd) == function ]; then
  log_cmd() {
    echo ">>> $*"
    $*
  }
fi

if [ ! $(type -t log_error) == function ]; then
  log_error() {
    echo $*
  }
fi

CONFIG=.gitconfig
SOURCE=$PWD/$CONFIG
TARGET=$HOME/$CONFIG
BACKUP=$TARGET.bak

if [ -L $TARGET ] && [ $(readlink -f $TARGET) == $SOURCE ]; then
  echo installation has already been done
  exit 0
fi

if [ -e $TARGET ] || [ -L $TARGET ]; then
  log_cmd mv $TARGET $BACKUP
  code=$?
  if [ $code -ne 0 ]; then
    log_error command failed!
    exit 1
  fi
fi

log_cmd ln -s $SOURCE $TARGET
code=$?
if [ $code -ne 0 ]; then
  log_error command failed!
  exit 1
fi

exit 0
