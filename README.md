# Git Configuration

My configuration for [Git](https://git-scm.com)

## 🛠️ Installation

You can use instructions below, but for best experience use [MetaDot](https://gitlab.com/dot-files42/metadot)

### Clone the repository

```shell
git clone git@gitlab.com:dot-files42/git.git ~/.dotfiles/git
```

### Automatic install

```shell
cd ~/.dotfiles/git
./install.sh
```

### Manual install

#### Make a backup of your current Git configuration

```shell
mv ~/.gitconfig ~/.gitconfig.bak
```

#### Create link to config file

```shell
ln -s ~/.dotfiles/git/.gitconfig ~/.gitconfig
```

## Use your Git

```shell
git
```
